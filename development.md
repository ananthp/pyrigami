# Pyrigami - Development

## TODO

- [ ] readme
- [ ] docstrings

## Setup

First time after cloning:
* `poetry install` to install project (and dev) dependencies in a venv for the project.
* `poetry shell` to activate virtual env.
* This project uses git hooks by `pre-commit`. Run `pre-commit install`.
    - `pre-commit` itself is a dev dependency, installed in venv along with others by poetry.

Afterwards:
* `poetry shell` to activate venv with project dependencies (including dev deps).
    - Launch vim in this shell. venv with project dependencies neededs to be active for autocompletion and other smartness work.
    - Python packages required by vim and its plugins (`pynvim`, `jedi`, ...) are also added as deps.
* `pytest` to run tests.

Updates:
* `poetry update` to update deps.
    - Sometimes, may need to delete `poetry.lock` and run `poetry install`.
    - Package versions are constrained in `pyproject.toml`, usually to update within a major version. Update this file to change upgrade major version of any package.
* `poetry self update` to update poetry itself.
* `pre-commit autoupdate` to update libs used in pre-commit, such as `black` and `isort`.
    - These libs are not part of project/dev deps. `pre-commit` maintains them separately.

## Tools, Libs, Docs

* [Poetry](https://python-poetry.org/docs/)
* [pre-commit](https://pre-commit.com/)
* [pydantic](https://pydantic-docs.helpmanual.io/)
* [black](https://black.readthedocs.io/) formats code (pre-commit hook)
* [isort](https://pycqa.github.io/isort/) sorts imports (pre-commit hook)

## Notes

### Data modeling

[statham](https://statham-schema.readthedocs.io/en/latest/index.html) instead of pydantic to define model/json-schema?

### SVG

SVG creation libraries, in python:
- [svgwrite](https://github.com/mozman/svgwrite)? -- but development stopped

in other languages:
- [snap.svg](http://snapsvg.io)
- <https://github.com/simmone/racket-simple-svg/>

SVG -> PDF/image converters:
- [librsvg](https://wiki.gnome.org/action/show/Projects/LibRsvg)
    - [This](https://pypi.org/project/sphinxcontrib-svg2pdfconverter/) talks about `rsvg-convert` cli tool from librsvg
    - <https://gitlab.gnome.org/GNOME/librsvg/-/blob/main/src/bin/rsvg-convert.rs>
- [CairoSVG](https://cairosvg.org/)

misc:
- [pydyf](https://doc.courtbouillon.org/pydyf/latest/common_use_cases.html) - low level pdf creation library.
- [svgelements](https://github.com/meerk40t/svgelements) - among other things, provides "SVG spec-like elements and structures".
- [flat](https://xxyxyz.org/flat) "creating and manipulating digital forms of fine arts"

SVG defines interfaces. See code here: <https://www.w3.org/TR/SVG2/types.html#InterfaceSVGLength>. Is there any official C/C++ code repo? If so the definitions etc can be translated to python with SWIG.

If there's no official impl of SVG definitions, can adapt from inkscape or mozilla code? See <https://gitlab.com/inkscape/inkscape/-/blob/master/src/svg/svg-length.h>

Otherwise, can copy-paste from IDL snippets from SVG definition and adapt.
