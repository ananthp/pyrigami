"""
`engraver` takes a `score` and renders it as PDF, SVG or PNG file.

score is a collection of drawing elements and other necessary info.
describes the page, and what to draw.

score can be:
- a dict
- a json file, or json string
- Score object

json and dict should follow the schema defined by Score.
See score module for details.

dict/json are preferable over manually constructed Score object.
See the tests for examples.

json can be generated by tools written in other languages.
core logic of music score generators, calendar and other useful printables,
can be done in a language of choice (kotlin, rust, etc.),
and the final output can be rendered to pdf using this project.
(python has good cairo binding, better than other language bindings).

Score is modeled using pydantic. This makes serializing and deserializing
to/from json easy. This also produces the necessary json schema.

finally,

    engraver.render(score) # produces pdf/png/svg file

"""

import os

import qahirah as qah

from pyrigami import score


class Engraver(object):
    def __init__(self, drawing: score.Score):
        self.drawing = drawing

    def engrave(self):
        pass


class CairoEngraver(Engraver):
    def create_surface_context(self):
        w = self.drawing.page.width
        h = self.drawing.page.height
        filename = self.drawing.file

        surface = None
        if filename.endswith("pdf"):
            surface = qah.PDFSurface
        elif filename.endswith("svg"):
            surface = qah.SVGSurface

        self.surf = surface.create(filename=filename, dimensions_in_points=(w, h))
        self.ctx = qah.Context.create(self.surf)

    def set_color(self, color):
        # pydantic generates CSS style RGB tuple,
        # each value ranging from 0 - 255.
        # cairo/qahirah expects each value to be in 0 - 1.
        tuple_range_255 = color.as_rgb_tuple()
        tuple_range_1 = (x / 255 for x in tuple_range_255)
        self.ctx.source_colour = tuple_range_1

    def set_stroke_style(self, style):
        stroke_width = style.stroke_width
        if stroke_width:
            self.ctx.line_width = stroke_width

        stroke = style.stroke
        if stroke:
            self.set_color(stroke)

        dasharray = style.stroke_dasharray
        dashoffset = style.stroke_dashoffset
        if dasharray:
            dashoffset = dashoffset or 0
            self.ctx.dash = (tuple(dasharray), dashoffset)

        linecap = style.stroke_linecap
        if linecap:
            self.ctx.line_cap = linecap.get_cairo_value()

    def set_fill_style(self, style):
        fill = style.fill
        if fill:
            self.set_color(fill)

    def set_font(self, style):
        font_face = style.font_family or "sans"
        font_size = style.font_size or 12
        font_style = (style.font_style or score.FontStyle.NORMAL).get_cairo_value()
        font_weight = (style.font_weight or score.FontWeight.NORMAL).get_cairo_value()

        font = (
            qah.Context.create_for_dummy()
            .select_font_face(font_face, font_style, font_weight)
            .set_font_size(font_size)
            .scaled_font
        )

        self.ctx.scaled_font = font

    def align_text(self, text_data):
        text_extents = self.ctx.text_extents(text_data.text)
        x, y = text_data.coords.x, text_data.coords.y
        match text_data.style.text_anchor:
            case None | score.TextAnchor.START:
                return (x, y)
            case score.TextAnchor.MIDDLE:
                return (x - text_extents.x_advance / 2, y)
            case score.TextAnchor.END:
                return (x - text_extents.x_advance, y)

    def draw_line(self, line_data):
        self.set_stroke_style(line_data.style)

        coords = line_data.coords
        x1, y1 = coords.x1, coords.y1
        x2, y2 = coords.x2, coords.y2
        self.ctx.move_to((x1, y1))
        self.ctx.line_to((x2, y2))

        self.ctx.stroke()

    def draw_text(self, text_data):
        self.set_fill_style(text_data.style)
        self.set_font(text_data.style)
        x, y = self.align_text(text_data)
        self.ctx.move_to((x, y))
        self.ctx.show_text(text_data.text)
        self.ctx.fill()

    def engrave(self):

        self.create_surface_context()

        for elem in self.drawing.draw:
            self.ctx.save()

            match elem:
                case score.Line():
                    self.draw_line(elem)
                case score.Text():
                    self.draw_text(elem)
                case _:
                    raise ValueError(f"Element type not supported yet: {elem}")

            # ensures settings from elements don't stick to subsequent elements.
            self.ctx.restore()

        self.surf.show_page()

        # flush to trigger actual output
        self.surf = self.ctx = None
