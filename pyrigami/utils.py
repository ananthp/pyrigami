from enum import Enum


def _transform_underscores_to_dashes(string: str) -> str:
    return string.replace("_", "-")


class LowercaseStringEnum(Enum):
    def _generate_next_value_(name, start, count, last_values):
        return name.lower()
