from dataclasses import dataclass

from pyrigami.units import MeasurementUnit, Mm


@dataclass
class Paper:
    width: MeasurementUnit
    height: MeasurementUnit

    def __init__(self, width, height):
        self.width = width
        self.height = height

        self._short_side, self._long_side = tuple(sorted((width, height)))

    def portrait(self):
        """returns a new instance in portrait mode"""
        return Paper(width=self._short_side, height=self._long_side)

    def landscape(self):
        """returns a new instance in landscape mode"""
        return Paper(width=self._long_side, height=self._short_side)


A4 = Paper(Mm(210), Mm(297))
