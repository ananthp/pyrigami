from pytest import approx

from pyrigami.score import Line, Page, Score, from_dict
from tests.samples import score_dict


def test_convert_dict_to_score_model():
    score = from_dict(score_dict)
    assert isinstance(score, Score)
    assert isinstance(score.page, Page)
    assert isinstance(score.draw[0], Line)


def test_roundtrip_conversion():
    # conversion from dict to object
    score = from_dict(score_dict)
    # pytest.approx can be used to compare floats,
    # dictionaries with float values.
    # pytest.approx doesn't support nested dicts yet.
    # when it does, this test can be written like so:
    # assert score.dict() == approx(score_dict)
    #
    # until then, checking a few random keys.
    # alternatively, we can write our own nested approx
    # see: https://stackoverflow.com/a/56048692/308123

    # conversion back to dict from object
    score_dict1 = score.dict()

    assert score_dict["page"] == approx(score_dict1["page"])
    assert score_dict["file"] == score_dict1["file"]
    assert score_dict["draw"][0]["coords"] == approx(score_dict1["draw"][0]["coords"])
