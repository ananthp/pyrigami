from pytest import approx

from pyrigami.units.paper import A4


def test_a4_size():
    # ref: inkscape
    A4_LONG_SIDE_PTS = 841.88977
    A4_SHORT_SIDE_PTS = 595.27557

    A4_PORTRAIT = A4.portrait()
    A4_LANDSCAPE = A4.landscape()

    assert A4_PORTRAIT.width.to_pt().value == approx(A4_SHORT_SIDE_PTS)
    assert A4_PORTRAIT.height.to_pt().value == approx(A4_LONG_SIDE_PTS)
    assert A4_LANDSCAPE.width.to_pt().value == approx(A4_LONG_SIDE_PTS)
    assert A4_LANDSCAPE.height.to_pt().value == approx(A4_SHORT_SIDE_PTS)
