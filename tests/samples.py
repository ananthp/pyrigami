# score constructed as a dict.
# this can also be created by importing a json.
# should follow schema defined by pyrigami.engraver.score
from pyrigami.score import Score, Page, Line, LineCoords, Style

score_dict = {
    "page": {"width": 200, "height": 200},
    "file": "/tmp/pyrigami-test-generated-kdiw528x.pdf",
    "draw": [
        {
            "element": "line",
            "coords": {"x1": 20, "y1": 20, "x2": 20, "y2": 180},
            "style": {"stroke": "#0277BD", "stroke-width": 2},
        },
        {
            "element": "line",
            "coords": {"x1": 30, "y1": 20, "x2": 30, "y2": 180},
            "style": {
                "stroke": "#3E2723",
                "stroke-width": 2,
                "stroke-dasharray": [10, 3],
                "stroke-dashoffset": 6,
            },
        },
        {
            "element": "line",
            "coords": {"x1": 40, "y1": 20, "x2": 40, "y2": 180},
            "style": {
                "stroke": "rgb(50,50,50)",
                "stroke-width": 0.5,
            },
        },
        {
            "element": "line",
            "coords": {"x1": 50, "y1": 20, "x2": 50, "y2": 180},
            "style": {
                "stroke": "#26A69A",
                "stroke-width": 2,
                "stroke-linecap": "butt",
                "stroke-dasharray": [10, 5],
            },
        },
        {
            "element": "line",
            "coords": {"x1": 55, "y1": 20, "x2": 55, "y2": 180},
            "style": {
                "stroke": "#26A69A",
                "stroke-width": 2,
                "stroke-linecap": "round",
                "stroke-dasharray": [10, 5],
            },
        },
        {
            "element": "line",
            "coords": {"x1": 60, "y1": 20, "x2": 60, "y2": 180},
            "style": {
                "stroke": "#26A69A",
                "stroke-width": 2,
                "stroke-linecap": "square",
                "stroke-dasharray": [10, 5],
            },
        },
        {
            "element": "text",
            "id": "normal left-aligned text",
            "text": "hello",
            "coords": {"x": 100, "y": 50},
            "style": {
                "fill": "black",
                "font-family": "sans",
                "font-style": "normal",
                "font-weight": "normal",
                "font-size": 10,
                "text-anchor": "start",
            },
        },
        {
            "element": "text",
            "id": "italic center-aligned text",
            "text": "hello",
            "coords": {"x": 100, "y": 70},
            "style": {
                "fill": "black",
                "font-family": "sans",
                "font-style": "italic",
                "font-weight": "normal",
                "font-size": 10,
                "text-anchor": "middle",
            },
        },
        {
            "element": "text",
            "id": "bold, right-aligned text",
            "text": "hello",
            "coords": {"x": 100, "y": 90},
            "style": {
                "fill": "#CDDC39",
                "font-family": "sans",
                "font-style": "normal",
                "font-weight": "bold",
                "font-size": 10,
                "text-anchor": "end",
            },
        },
    ],
}

score_obj = Score(
    page=Page(width=200, height=200),
    file="/tmp/pyrigami-test-generated-cdi9j2.pdf",
    draw=[
        Line(
            coords=LineCoords(x1=20, y1=20, x2=20, y2=180),
            style=Style(stroke_width=1, stroke="rgb(30,50,255)"),
        ),
        Line(
            coords=LineCoords(x1=30, y1=20, x2=30, y2=180),
            style=Style(stroke_width=1, stroke="#FF7043", stroke_dasharray=[10, 3]),
        ),
        Line(
            coords=LineCoords(x1=40, y1=20, x2=40, y2=180),
            # TODO BUG alpha doesn't work.
            style=Style(stroke_width=1, stroke="rgba(0,0,0, 0.99)"),
        ),
    ],
)
