import json
from os import remove
from os.path import exists

from pyrigami import score
from tests.samples import score_dict, score_obj


def remove_if_exists(filename):
    if exists(filename):
        remove(filename)


def test_engraver_renders_score_from_dict():
    outfile = score_dict["file"]
    remove_if_exists(outfile)

    score.from_dict(score_dict).render()
    assert exists(outfile)


def test_engraver_renders_score_from_obj():
    outfile = score_obj.file
    remove_if_exists(outfile)

    score_obj.render()
    assert exists(outfile)


def test_engraver_renders_score_from_json_str():
    score_json_str = json.dumps(score_dict)
    outfile = score_dict["file"]
    remove_if_exists(outfile)

    score.from_json_str(score_json_str).render()
    assert exists(outfile)


def test_engraver_renders_score_from_json_file():
    json_file = "score-demo.json"

    outfile = None
    with open(json_file) as f:
        score_dict = json.load(f)
        outfile = score_dict["file"]
        remove_if_exists(outfile)

    score.from_json_file(json_file).render()
    assert exists(outfile)
