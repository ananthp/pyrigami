import pytest
from pytest import approx

from pyrigami.units import Consts, In, Mm, Pt, UnitArithmeticError


def test_measurement_conversions():
    assert In(1).to_mm() == approx(Mm(25.4))
    assert In(1).to_pt() == approx(Pt(72))
    assert Mm(25.4).to_in() == approx(In(1))
    assert Pt(72).to_in() == approx(In(1))


def test_can_add_subtract_units():
    assert In(2) == approx(In(1) + In(1))
    assert In(3) == approx(In(1) + In(1) + In(1))
    assert In(2) == approx(In(3) - In(1))
    assert In(2) == approx(In(4) - In(1) - In(1))

    # small Mm values result in floating point approximations
    # making it tricky to test.
    assert Mm(400) == approx(Mm(200) + Mm(200))
    assert Mm(30) == approx(Mm(10) + Mm(10) + Mm(10))
    assert Mm(200) == approx(Mm(300) - Mm(100))
    assert Mm(200) == approx(Mm(400) - Mm(100) - Mm(100))

    assert Pt(2) == approx(Pt(1) + Pt(1))
    assert Pt(3) == approx(Pt(1) + Pt(1) + Pt(1))
    assert Pt(2) == approx(Pt(3) - Pt(1))
    assert Pt(2) == approx(Pt(4) - Pt(1) - Pt(1))

    assert In(2) == approx(In(1) + Mm(Consts.MM_PER_INCH.value))
    assert In(2) == approx(
        (Mm(Consts.MM_PER_INCH.value) + Mm(Consts.MM_PER_INCH.value)).to_in()
    )
    assert In(3) == approx(
        (In(1) + Pt(Consts.PT_PER_INCH.value) + Mm(Consts.MM_PER_INCH.value)).to_in()
    )


def test_can_multiply_divide_units_by_scalars():
    assert In(4) == approx(In(2) * 2)
    assert In(4) == approx(In(8) / 2)
    assert In(3) == approx(In(10) // 3)

    assert Mm(4) == approx(Mm(2) * 2)
    assert Mm(4) == approx(Mm(8) / 2)
    assert Mm(3) == approx(Mm(10) // 3)

    assert Pt(4) == approx(Pt(2) * 2)
    assert Pt(4) == approx(Pt(8) / 2)
    assert Pt(3) == approx(Pt(10) // 3)


def test_doesnt_allow_addition_subtraction_with_scalar():
    with pytest.raises(UnitArithmeticError):
        Mm(100) + 5
    with pytest.raises(UnitArithmeticError):
        Mm(100) - 5

    with pytest.raises(UnitArithmeticError):
        Pt(100) + 5
    with pytest.raises(UnitArithmeticError):
        Pt(100) - 5

    with pytest.raises(UnitArithmeticError):
        In(100) + 5
    with pytest.raises(UnitArithmeticError):
        In(100) - 5


def test_doesnt_allow_mul_div_among_units():
    with pytest.raises(UnitArithmeticError):
        Mm(100) * Mm(5)
    with pytest.raises(UnitArithmeticError):
        Mm(100) / Mm(5)
    with pytest.raises(UnitArithmeticError):
        Mm(100) // Mm(5)

    with pytest.raises(UnitArithmeticError):
        Pt(100) * Pt(5)
    with pytest.raises(UnitArithmeticError):
        Pt(100) / Pt(5)
    with pytest.raises(UnitArithmeticError):
        Pt(100) // Pt(5)

    with pytest.raises(UnitArithmeticError):
        In(100) * In(5)
    with pytest.raises(UnitArithmeticError):
        In(100) / In(5)
    with pytest.raises(UnitArithmeticError):
        In(100) // In(5)
