#!/bin/env python3

from pyrigami.score import Score

# TODO tweak schema
# - remove unnecessary 'title's
# - add $schema, $id (consider versioning)
# - add documentation in model
# - ref:
#   - https://pydantic-docs.helpmanual.io/usage/schema/
#   - https://stackoverflow.com/questions/69980323/how-to-include-id-fields-in-pydantic-schema
#   - https://json-schema.org/learn/getting-started-step-by-step.html
print(Score.schema_json(indent=4))
